package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {
    @Test
    void testStandartItemConstructor(){
        StandardItem sItem = new StandardItem(1, "s001", 1, "category_001", 10);
        assertEquals(sItem.getID(), 1);
        assertEquals(sItem.getName(), "s001");
        assertEquals(sItem.getPrice(), 1);
        assertEquals(sItem.getCategory(), "category_001");
        assertEquals(sItem.getLoyaltyPoints(), 10);
    }
    @Test
    public void testStandartItemCopy() {
        StandardItem sitem1 = new StandardItem(1, "s001", 1, "category_001", 10);
        StandardItem copyItem = sitem1.copy();
        assertEquals(sitem1.getID(), copyItem.getID());
        assertEquals(sitem1.getName(), copyItem.getName());
        assertEquals(sitem1.getPrice(), copyItem.getPrice(), 0.01);
        assertEquals(sitem1.getCategory(), copyItem.getCategory());
        assertEquals(sitem1.getLoyaltyPoints(), copyItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource(
            value = {"1;'s_1';1;'cat_1';1", "2;'s_2';2;'cat_2';2", "3;'s_3';3;'cat_3';3" },
            delimiter = ';'
    )
    void testStandartItemEquals(int id, String name, float price, String category, int loyalityPoints){
        StandardItem sItem1 = new StandardItem(id, name, price, category, loyalityPoints);
        StandardItem sItem2 = new StandardItem(id, name, price, category, loyalityPoints);
        assertEquals(true, sItem1.equals(sItem2));

    }

}