package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.storage.ItemStock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    @Test
    void testItemStockConstructor(){
        Item item = new Item(1, "s001", 1, "category_001");
        ItemStock itemStock = new ItemStock(item);
        assertEquals(item, itemStock.getItem());
    }


    @ParameterizedTest
    @CsvSource(
            value = {"1;'s_1';1;'cat_1';1;-2", "2;'s_2';2;'cat_2';2;-5", "3;'s_3';3;'cat_3';3;-3" },
            delimiter = ';'
    )
    void testItemStockIncreaseItemCount(int id, String name, float price, String category, int count1, int count2){
        Item item = new Item(1, "s001", 1, "category_001");
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(count1);
        itemStock.decreaseItemCount(count2);
        assertEquals(itemStock.getCount(), count1-count2 );
    }
    @ParameterizedTest
    @CsvSource(
            value = {"1;'s_1';1;'cat_1';10;5", "2;'s_2';2;'cat_2';-12;13", "3;'s_3';3;'cat_3';-3;0" },
            delimiter = ';'
    )
    void testItemStockDecreaseItemCount(int id, String name, float price, String category, int count1, int count2){
        Item item = new Item(1, "s001", 1, "category_001");
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(count1);
        itemStock.decreaseItemCount(count2);
        assertEquals(itemStock.getCount(), count1-count2 );
    }

}