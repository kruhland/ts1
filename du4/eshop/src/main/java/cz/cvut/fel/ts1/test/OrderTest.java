package cz.cvut.fel.ts1.test;

import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testOrderConstructor(){
        ShoppingCart cart1 = new ShoppingCart();
        Order order = new Order(cart1, "name1", "adress1", 10);
        assertEquals(cart1.getCartItems(), order.getItems());
        assertEquals("name1", order.getCustomerName());
        assertEquals("adress1", order.getCustomerAddress());
        assertEquals(10, order.getState());
    }
    @Test
    void testOrderNullConstructor(){
        ShoppingCart cart1 = new ShoppingCart();
        Order order = new Order(cart1, "name1", "adress1");
        assertEquals(cart1.getCartItems(), order.getItems());
        assertEquals("name1", order.getCustomerName());
        assertEquals("adress1", order.getCustomerAddress());
        assertEquals(0, order.getState());

    }
}