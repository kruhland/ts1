
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainTest {
    private static WebDriver driver;

    @BeforeAll
    static void setUp() {
        driver = new ChromeDriver();
//        driver.get("https://link.springer.com/");


    }




    @ParameterizedTest      //email and password
    @CsvSource({
           ""

               })
    void testLogin(String email, String password){
        driver.get("https://link.springer.com/");

        //reject cookies main page
        WebElement cookiesButton = driver.findElement(By.xpath("/html/body/dialog/div[2]/div/div[2]/button[2]"));
        cookiesButton.click();

        //click login
        WebElement LoginButton = driver.findElement(By.xpath("//*[@id=\"identity-account-widget\"]/span"));
        LoginButton.click();

        //reject privacy cookies
        WebElement privacyCookiesButton = driver.findElement(By.xpath("/html/body/section/div/div[2]/button[2]"));
        privacyCookiesButton.click();

        //send email keys
        WebElement emailInput = driver.findElement((By.xpath("//*[@id=\"login-email\"]")));
        emailInput.sendKeys(email);

        //click submit button
        WebElement emailSubmitButton = driver.findElement(By.xpath("//*[@id=\"email-submit\"]"));
        emailSubmitButton.click();

        //send password keys
        WebElement passwordInput = driver.findElement((By.xpath("//*[@id=\"login-password\"]")));
        passwordInput.sendKeys(password);

        //click submit
        WebElement passwordSubmitButton = driver.findElement(By.xpath("//*[@id=\"password-submit\"]"));
        passwordSubmitButton.click();

    }
        //advanced search

    @Test
    void testAdvancedSearch() {
        driver.get("https://link.springer.com/advanced-search ");

        WebElement input1 = driver.findElement(By.xpath("//*[@id=\"all-words\"]"));
        input1.sendKeys("Page Object Model");

        WebElement input2 = driver.findElement(By.xpath("//*[@id=\"least-words\"]"));
        input2.sendKeys("Sellenium Testing");

        WebElement input3 = driver.findElement(By.xpath("//*[@id=\"facet-start-year\"]"));
        input3.sendKeys("2024");

        WebElement input4 = driver.findElement(By.xpath("//*[@id=\"facet-end-year\"]"));
        input4.sendKeys("2024");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"submit-advanced-search\"]"));
        searchButton.click();

        //update advanced search
        WebElement updateArticle = driver.findElement(By.xpath("//*[@id=\"list-content-type-filter\"]/li[2]/div/label/span/span[1]"));
        updateArticle.click();

        WebElement updateArticleSearchButton = driver.findElement(By.xpath("//*[@id=\"popup-filters\"]/div[3]/button[2]"));
        updateArticleSearchButton.click();

        ArrayList<String> articleNamesArray = new ArrayList<>(4);
        ArrayList<String> articleDateArray = new ArrayList<>(4);
        ArrayList<String> articleDOIArray = new ArrayList<>(4);

        for(int i = 1; i<5; i++){

            if(i != 4) {
                String articleName = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3/a")).getText();
                String articleDate = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/div[3]/div/span[2]")).getText();
                String articleDOI = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3/a")).getAttribute("href");
                articleNamesArray.add(articleName);
                articleDateArray.add(articleDate);
                articleDOIArray.add(articleDOI);
            }
            else{
                String articleName = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3/a")).getText();
                String articleDate = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/div[3]/div/span[3]")).getText();
                String articleDOI = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[" + i + "]/div[1]/div/h3/a")).getAttribute("href");
                articleNamesArray.add(articleName);
                articleDateArray.add(articleDate);
                articleDOIArray.add(articleDOI);
            }



        }

        String articleName = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/h3/a")).getText();
        String articleDate = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/div[3]/div/span[2]")).getText();
        String articleDOI = driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/h3/a")).getAttribute("href");



        System.out.println(articleNamesArray);
        System.out.println(articleDateArray);
        System.out.println(articleDOIArray);



        driver.get("https://link.springer.com/search");

//        for(int i = 1; i<5; i++){
//            driver.findElement(By.xpath("//*[@id=\"search-springerlink\"]")).sendKeys(articleNamesArray.get(i));
//            driver.findElement(By.xpath("//*[@id=\"search-submit\"]")).click();
//            driver.findElement(By.xpath("//*[@id=\"search-springerlink\"]")).clear();
//        }

        for (int i = 1; i <= 4; i++) {
            driver.get("https://link.springer.com/search");
            driver.findElement(By.xpath("//*[@id=\"search-springerlink\"]")).sendKeys(articleNamesArray.get(i - 1));
            driver.findElement(By.xpath("//*[@id=\"search-submit\"]")).click();

            Assertions.assertEquals(articleDOIArray.get(i - 1),  driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/h3/a")).getAttribute("href"));

            if (i == 4) {
                Assertions.assertEquals(articleDateArray.get(i - 1), driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/div[3]/div/span[3]")).getText());
            } else {
                Assertions.assertEquals(articleDateArray.get(i - 1), driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div/div[2]/div[2]/ol/li[1]/div[1]/div/div[3]/div/span[2]")).getText());
            }


        }
        driver.findElement(By.xpath("//*[@id=\"search-springerlink\"]")).clear();

    }

    @Test
    void testLogout(){
        driver.findElement(By.xpath("//*[@id=\"identity-account-widget\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"account-nav-menu\"]/ul/li[4]/a")).click();
        //driver.quit();
    }
    }



